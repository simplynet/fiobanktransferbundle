<?php

/*
* This file is part of the Fio Bank Account Bundle.
*
* (c) Petr Jaroš <petajaros@gmail.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace SimplyNet\FioBankTransferBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class Configuration
{
    public function getConfigTree()
    {
        $tb = new TreeBuilder();

        return $tb
            ->root('simply_net_fio_bank_transfer', 'array')
                ->children()
                    ->scalarNode('token')->isRequired()->cannotBeEmpty()->end()
                    ->booleanNode('root_certificate')->defaultValue('Resources/certificates/cacert.pem')->end()
                    ->scalarNode('expiration_period')->defaultValue('P14D')->end()
                ->end()
            ->end()
            ->buildTree();
    }
}
