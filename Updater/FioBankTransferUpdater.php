<?php

/*
* This file is part of the Fio Bank Account Bundle.
*
* (c) Petr Jaroš <petajaros@gmail.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace SimplyNet\FioBankTransferBundle\Updater;

use JMS\Payment\CoreBundle\Model\FinancialTransactionInterface;
use JMS\Payment\CoreBundle\Model\PaymentInterface;
use JMS\Payment\CoreBundle\Model\PaymentInstructionInterface;
use JMS\Payment\CoreBundle\PluginController\PluginController;
use JMS\Payment\CoreBundle\Entity\PaymentInstruction;
use SimplyNet\FioBankTransferBundle\Client\Client;
use SimplyNet\FioBankTransferBundle\TransactionMatcher\TransactionMatcherInterface;
use Doctrine\ORM\EntityManager;

class FioBankTransferUpdater
{
    /**
     * @var \JMS\Payment\CoreBundle\PluginController\PluginController
     */
    private $pluginController;

    /**
     * @var \SimplyNet\FioBankTransferBundle\Client\Client
     */
    private $client;

    /**
     * @var \SimplyNet\FioBankTransferBundle\TransactionMatcher\TransactionMatcherInterface;
     */
    private $transactionMatcher;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     *
     * @param \JMS\Payment\CoreBundle\PluginController\PluginController
     * @param \SimplyNet\FioBankTransferBundle\Client\Client $client
     * @param \SimplyNet\FioBankTransferBundle\TransactionMatcher\TransactionMatcherInterface $transactionMatcher
     */
    function __construct(PluginController $pluginController, Client $client, TransactionMatcherInterface $transactionMatcher, EntityManager $em)
    {
        $this->pluginController = $pluginController;
        $this->client = $client;
        $this->transactionMatcher = $transactionMatcher;
        $this->em = $em;
    }
    
    /**
     *
     * @return array
     */
    public function updatePayments(&$processedTransactions, &$unprocessedTransactions)
    {
        $financialTransactions = $this->getPendingTransactions();
        $processedTransactions = array();
        $unprocessedTransactions = $this->client->getLastTransactions();

        /* @var $financialTransaction \JMS\Payment\CoreBundle\Entity\FinancialTransaction */
        foreach($financialTransactions as $financialTransaction) {
            $payment = $financialTransaction->getPayment();
            if($payment->isExpired()) {
                $payment->setState(PaymentInterface::STATE_EXPIRED);
                $this->em->persist($payment);
            } else {
                /* @var $transaction \SimplyNet\FioBankTransferBundle\Client\Transaction */
                foreach($unprocessedTransactions as $key => $transaction) {
                    if($this->transactionMatcher->match($transaction, $payment)) {
                        $financialTransaction->setReferenceNumber($transaction->getId());
                        $this->em->persist($financialTransaction);
                        $this->em->flush();
                        $this->pluginController->approveAndDeposit($payment, $transaction->getAmount());
                        $processedTransactions[] = $transaction;
                        unset($unprocessedTransactions[$key]);
                        break;
                    }
                }
            }
        }
        $this->em->flush();
    }
    
    /**
     * 
     * @param $transaction \SimplyNet\FioBankTransferBundle\Client\Transaction
     */
    public function createPayment($transaction)
    {
        if(!$this->isPaymentProcessed($transaction->getId())) {
            $paymentInstruction = new PaymentInstruction($transaction->getAmount(), 'CZK', 'fio_bank_transfer');
            $this->pluginController->createPaymentInstruction($paymentInstruction);
            /* @var $payment \JMS\Payment\CoreBundle\Entity\Payment */
            $payment = $this->pluginController->createPayment($paymentInstruction->getId(), $transaction->getAmount());
            $this->pluginController->approveAndDeposit($payment->getId(), $transaction->getAmount());
            $payment->getApproveTransaction()->setReferenceNumber($transaction->getId());
            $this->pluginController->approveAndDeposit($payment->getId(), $transaction->getAmount());
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    private function getPendingTransactions()
    {
        $qb = $this->em->createQueryBuilder();

        $qb->select('p', 'pi', 't');
        $qb->from('JMSPaymentCoreBundle:FinancialTransaction', 't');
        $qb->leftJoin('t.payment', 'p');
        $qb->leftJoin('p.paymentInstruction', 'pi');
        $qb->where('t.transactionType = :transactionType')->setParameter('transactionType', FinancialTransactionInterface::TRANSACTION_TYPE_APPROVE_AND_DEPOSIT);
        $qb->andWhere('t.state = :transactionState')->setParameter('transactionState', FinancialTransactionInterface::STATE_PENDING);
        $qb->andWhere('pi.paymentSystemName = :paymentSystemName')->setParameter('paymentSystemName', 'fio_bank_transfer');
        $qb->andWhere('pi.state = :paymentInstructionState')->setParameter('paymentInstructionState', PaymentInstructionInterface::STATE_VALID);
        $qb->andWhere('p.state = :paymentstate')->setParameter('paymentstate', PaymentInterface::STATE_APPROVING);
        $q = $qb->getQuery(); /* @var $q \Doctrine\ORM\Query */

        return $q->execute();
    }
    
    /**
     * 
     * @param string $id
     * @return bool
     */
    private function isPaymentProcessed($id)
    {
        $qb = $this->em->createQueryBuilder();

        $qb->select('count(t.id)');
        $qb->from('JMSPaymentCoreBundle:FinancialTransaction', 't');
        $qb->where('t.referenceNumber = :referenceNumber')->setParameter('referenceNumber', $id);
        $q = $qb->getQuery(); /* @var $q \Doctrine\ORM\Query */

        return $q->getSingleScalarResult() > 0;
    }
}
