<?php

/*
* This file is part of the Fio Bank Account Bundle.
*
* (c) Petr Jaroš <petajaros@gmail.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace SimplyNet\FioBankTransferBundle\Plugin;

use JMS\Payment\CoreBundle\Plugin\AbstractPlugin;
use JMS\Payment\CoreBundle\Model\FinancialTransactionInterface;
use JMS\Payment\CoreBundle\Plugin\PluginInterface;
use JMS\Payment\CoreBundle\Plugin\Exception\PaymentPendingException;
use SimplyNet\FioBankTransferBundle\TransactionMatcher\TransactionMatcherInterface;

class FioBankTransferPlugin extends AbstractPlugin
{

    /**
     * @var \SimplyNet\FioBankTransferBundle\TransactionMatcher\TransactionMatcherInterface;
     */
    private $transactionMatcher;

    /**
     * @var string
     */
    private $expirationPeriod;

    /**
     *
     * @param \JMS\Payment\CoreBundle\Model\TransactionMatcherInterface $transactionMatcher
     * @param string $expirationPeriod
     * @param bool $isDebug
     */
    function __construct(TransactionMatcherInterface $transactionMatcher, $expirationPeriod, $isDebug = false)
    {
        $this->transactionMatcher = $transactionMatcher;
        $this->expirationPeriod = $expirationPeriod;
        parent::__construct($isDebug);
    }

    /**
     * {@inheritDoc}
     */
    public function approveAndDeposit(FinancialTransactionInterface $transaction, $retry)
    {
        if($transaction->getReferenceNumber() === null) {
            $expirationDate = new \DateTime();
            $expirationDate->add(new \DateInterval($this->expirationPeriod));
            $transaction->getPayment()->setExpirationDate($expirationDate);
            if(!$retry) {
                $this->transactionMatcher->markPaymentInstruction($transaction->getPayment()->getPaymentInstruction());
            }
            throw new PaymentPendingException();
        } else {
            $transaction->setProcessedAmount($transaction->getRequestedAmount());
            $transaction->setResponseCode(PluginInterface::RESPONSE_CODE_SUCCESS);
            $transaction->setReasonCode(PluginInterface::REASON_CODE_SUCCESS);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function processes($method)
    {
        return 'fio_bank_transfer' === $method;
    }
}
