<?php

/* 
 * This file is part of the Fio Bank Account Bundle.
 * 
 * (c) Petr Jaroš <petajaros@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SimplyNet\FioBankTransferBundle\TransactionMatcher;

use SimplyNet\FioBankTransferBundle\Client\Transaction;
use JMS\Payment\CoreBundle\Model\PaymentInstructionInterface;
use JMS\Payment\CoreBundle\Model\PaymentInterface;

interface TransactionMatcherInterface
{
    /**
     * Mark payment instruction for later matching with transaction feed.
     *
     * @param PaymentInstructionInterface $paymentInstruction
     */
    public function markPaymentInstruction(PaymentInstructionInterface $paymentInstruction);

    /**
     * Returns true when the bank transaction provided match the payment.
     *
     * @param Transaction $transaction
     * @param PaymentInterface $payment
     * @return bool
     */
    public function match(Transaction $transaction, PaymentInterface $payment);
}
