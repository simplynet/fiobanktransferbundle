<?php

/* 
 * This file is part of the Fio Bank Account Bundle.
 * 
 * (c) Petr Jaroš <petajaros@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SimplyNet\FioBankTransferBundle\TransactionMatcher;

use SimplyNet\FioBankTransferBundle\Client\Transaction;
use JMS\Payment\CoreBundle\Model\PaymentInstructionInterface;
use JMS\Payment\CoreBundle\Model\PaymentInterface;

class BasicTransactionMatcher implements TransactionMatcherInterface
{
    /**
     * {@inheritDoc}
     */
    public function markPaymentInstruction(PaymentInstructionInterface $paymentInstruction)
    {
        /* @var $extendedData \JMS\Payment\CoreBundle\Entity\ExtendedData */
        $extendedData = $paymentInstruction->getExtendedData();
        
        if(!$extendedData->has('variable_symbol')) {
            $extendedData->set('variable_symbol', $this->generateVariableSymbol());
        }
    }

    /**
     * {@inheritDoc}
     */
    public function match(Transaction $transaction, PaymentInterface $payment)
    {
        /* @var $extendedData \JMS\Payment\CoreBundle\Entity\ExtendedData */
        $extendedData = $payment->getPaymentInstruction()->getExtendedData();

        if($extendedData->has('variable_symbol') && ltrim($extendedData->get('variable_symbol'), ' 0') != ltrim($transaction->getVariableSymbol(), ' 0')) return false;
        if($extendedData->has('specific_symbol') && ltrim($extendedData->get('specific_symbol'), ' 0') != ltrim($transaction->getSpecificSymbol(), ' 0')) return false;
        if($transaction->getAmount() == 0) return false;
        if(abs(($payment->getDepositingAmount() - $transaction->getAmount()) / $transaction->getAmount()) > 0.00001) return false;
        return true;
    }

    /**
     *
     * @return string
     */
    private function generateVariableSymbol()
    {
        return rand(1, 99999) . rand(1, 99999);
    }

}
