<?php

/*
* This file is part of the Fio Bank Account Bundle.
*
* (c) Petr Jaroš <petajaros@gmail.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace SimplyNet\FioBankTransferBundle\Tests\Client;

use SimplyNet\FioBankTransferBundle\Client\Transaction;
use SimplyNet\FioBankTransferBundle\Client\Account;

class XmlClientTest extends \PHPUnit_Framework_TestCase
{
    public function testTransactionsByDate()
    {
        $xml = file_get_contents(__DIR__ . '/Fixtures/transactionsByDate.xml');

        $client = $this->getMock(
                '\SimplyNet\FioBankTransferBundle\Client\XmlClient',
                array('getRequestTransactionsByDate'),
                array('')
        );

        $client->method('getRequestTransactionsByDate')
                ->willReturn($xml);

        $transactions = $client->getTransactionsByDate(new \DateTime(), new \DateTime());

        $expectedTransactions = array();
        
        $transaction = new Transaction();
        $transaction->setId('1147608196');
        $transaction->setTimestamp(new \DateTime('2012-07-27'));
        $transaction->setAmount((float)-15.0);
        $transaction->setCurrency('CZK');
        $transaction->setUserIdentification('');
        $transaction->setType('Platba převodem uvnitř banky');
        $transaction->setPayer('Novák, Jan');
        $transaction->setNote('Můj test');
        $transaction->setOrderId('2102392862');
        $account = new Account();
        $account->setNumber('2111111111');
        $account->setBankId('2010');
        $account->setBankName('Fio banka, a.s.');
        $account->setBic('FIOBCZPPXXX');
        $account->setIban('CZ7920100000002111111111');
        $transaction->setSender($account);
        $account = new Account();
        $account->setNumber('2222233333');
        $account->setBankId('2010');
        $account->setBankName('Fio banka, a.s.');
        $transaction->setRecipient($account);

        $expectedTransactions[] = $transaction;

        $transaction = new Transaction();
        $transaction->setId('1147608197');
        $transaction->setTimestamp(new \DateTime('2012-07-27'));
        $transaction->setAmount((float)-20);
        $transaction->setCurrency('CZK');
        $transaction->setUserIdentification('');
        $transaction->setType('Platba převodem uvnitř banky');
        $transaction->setPayer('Novák, Jan');
        $transaction->setNote('');
        $transaction->setOrderId('2102392863');
        $account = new Account();
        $account->setNumber('2111111111');
        $account->setBankId('2010');
        $account->setBankName('Fio banka, a.s.');
        $account->setBic('FIOBCZPPXXX');
        $account->setIban('CZ7920100000002111111111');
        $transaction->setSender($account);
        $account = new Account();
        $account->setNumber('2222233333');
        $account->setBankId('2010');
        $account->setBankName('Fio banka, a.s.');
        $transaction->setRecipient($account);

        $expectedTransactions[] = $transaction;

        $this->assertEquals($expectedTransactions, $transactions);
    }

    public function testTransactionsByOrderNumber()
    {
        $xml = file_get_contents(__DIR__ . '/Fixtures/transactionsByOrderNumber.xml');

        $client = $this->getMock(
                '\SimplyNet\FioBankTransferBundle\Client\XmlClient',
                array('getRequestTransactionsByOrderNumber'),
                array('')
        );

        $client->method('getRequestTransactionsByOrderNumber')
                ->willReturn($xml);

        $transactions = $client->getTransactionsByOrderNumber(2012, 7);

        $expectedTransactions = array();

        $transaction = new Transaction();
        $transaction->setId('1147301403');
        $transaction->setTimestamp(new \DateTime('2012-06-30+02:00'));
        $transaction->setAmount((float)7.76);
        $transaction->setCurrency('CZK');
        $transaction->setType('Připsaný úrok');
        $transaction->setOrderId('2099310186');
        $account = new Account();
        $account->setNumber('2111111111');
        $account->setBankId('2010');
        $account->setBankName('Fio banka, a.s.');
        $account->setBic('FIOBCZPPXXX');
        $account->setIban('CZ7920100000002111111111');
        $transaction->setRecipient($account);

        $expectedTransactions[] = $transaction;

        $transaction = new Transaction();
        $transaction->setId('1147301404');
        $transaction->setTimestamp(new \DateTime('2012-06-30+02:00'));
        $transaction->setAmount((float)-1.00);
        $transaction->setCurrency('CZK');
        $transaction->setType('Odvod daně z úroků');
        $transaction->setOrderId('2099310186');
        $account = new Account();
        $account->setNumber('2111111111');
        $account->setBankId('2010');
        $account->setBankName('Fio banka, a.s.');
        $account->setBic('FIOBCZPPXXX');
        $account->setIban('CZ7920100000002111111111');
        $transaction->setSender($account);

        $expectedTransactions[] = $transaction;

        $this->assertEquals($expectedTransactions, $transactions);
    }

    public function testLastTransactions()
    {
        $xml = file_get_contents(__DIR__ . '/Fixtures/lastTransactions.xml');

        $client = $this->getMock(
                '\SimplyNet\FioBankTransferBundle\Client\XmlClient',
                array('getRequestLastTransactions'),
                array('')
        );

        $client->method('getRequestLastTransactions')
                ->willReturn($xml);

        $transactions = $client->getLastTransactions();

        $expectedTransactions = array();

        $transaction = new Transaction();
        $transaction->setId('1147608197');
        $transaction->setTimestamp(new \DateTime('2012-07-27+02:00'));
        $transaction->setAmount((float)-20.00);
        $transaction->setCurrency('CZK');
        $transaction->setUserIdentification('');
        $transaction->setType('Platba převodem uvnitř banky');
        $transaction->setPayer('Novák, Jan');
        $transaction->setNote('');
        $transaction->setOrderId('2102382863');
        $account = new Account();
        $account->setNumber('2111111111');
        $account->setBankId('2010');
        $account->setBankName('Fio banka, a.s.');
        $account->setBic('FIOBCZPPXXX');
        $account->setIban('CZ7920100000002111111111');
        $transaction->setSender($account);
        $account = new Account();
        $account->setNumber('2222233333');
        $account->setBankId('2010');
        $account->setBankName('Fio banka, a.s.');
        $transaction->setRecipient($account);

        $expectedTransactions[] = $transaction;

        $transaction = new Transaction();
        $transaction->setId('1147608198');
        $transaction->setTimestamp(new \DateTime('2012-07-27+02:00'));
        $transaction->setAmount((float)-352.00);
        $transaction->setCurrency('CZK');
        $transaction->setUserIdentification('');
        $transaction->setType('Platba převodem uvnitř banky');
        $transaction->setPayer('Novák, Jan');
        $transaction->setNote('');
        $transaction->setOrderId('2102382864');
        $account = new Account();
        $account->setNumber('2111111111');
        $account->setBankId('2010');
        $account->setBankName('Fio banka, a.s.');
        $account->setBic('FIOBCZPPXXX');
        $account->setIban('CZ7920100000002111111111');
        $transaction->setSender($account);
        $account = new Account();
        $account->setNumber('2222233333');
        $account->setBankId('2010');
        $account->setBankName('Fio banka, a.s.');
        $transaction->setRecipient($account);

        $expectedTransactions[] = $transaction;

        $this->assertEquals($expectedTransactions, $transactions);
    }

    public function testCompletelyFilledTransactions()
    {
        $xml = file_get_contents(__DIR__ . '/Fixtures/completelyFilledTransactions.xml');

        $client = $this->getMock(
                '\SimplyNet\FioBankTransferBundle\Client\XmlClient',
                array('getRequestTransactionsByDate'),
                array('')
        );

        $client->method('getRequestTransactionsByDate')
                ->willReturn($xml);

        $transactions = $client->getTransactionsByDate(new \DateTime(), new \DateTime());

        $expectedTransactions = array();

        $transaction = new Transaction();
        $transaction->setId('1147608197');
        $transaction->setTimestamp(new \DateTime('2012-07-27+02:00'));
        $transaction->setAmount((float)-20.0);
        $transaction->setCurrency('CZK');
        $transaction->setConstantSymbol('0558');
        $transaction->setVariableSymbol('45443455433');
        $transaction->setSpecificSymbol('3232323');
        $transaction->setUserIdentification('Nákup: PENNY MARKET s.r.o., Jaromer, CZ');
        $transaction->setSenderMessage('Děkuji');
        $transaction->setType('Platba převodem uvnitř banky');
        $transaction->setPayer('Novák, Jan');
        $transaction->setAdditionalInformation('15.90 EUR');
        $transaction->setNote('Hračky pro děti v PENNY MARKET');
        $transaction->setOrderId('2102382863');
        $account = new Account();
        $account->setNumber('2111111111');
        $account->setBankId('2010');
        $account->setBankName('Fio banka, a.s.');
        $account->setBic('FIOBCZPPXXX');
        $account->setIban('CZ7920100000002111111111');
        $transaction->setSender($account);
        $account = new Account();
        $account->setNumber('2222233333');
        $account->setName('Novák, Zdeněk');
        $account->setBankId('2010');
        $account->setBankName('Fio banka, a.s.');
        $account->setBic('UNCRITMMXXX');
        $transaction->setRecipient($account);

        $expectedTransactions[] = $transaction;

        $transaction = new Transaction();
        $transaction->setId('1147608198');
        $transaction->setTimestamp(new \DateTime('2012-07-27+02:00'));
        $transaction->setAmount((float)-352.00);
        $transaction->setCurrency('CZK');
        $transaction->setConstantSymbol('0559');
        $transaction->setVariableSymbol('3435555343');
        $transaction->setSpecificSymbol('123453');
        $transaction->setUserIdentification('');
        $transaction->setSenderMessage('Děkuji moc');
        $transaction->setType('Platba převodem uvnitř banky');
        $transaction->setPayer('Novák, Jan');
        $transaction->setAdditionalInformation('11.90 NZD');
        $transaction->setNote('');
        $transaction->setOrderId('2102382864');
        $account = new Account();
        $account->setNumber('2111111111');
        $account->setBankId('2010');
        $account->setBankName('Fio banka, a.s.');
        $account->setBic('FIOBCZPPXXX');
        $account->setIban('CZ7920100000002111111111');
        $transaction->setSender($account);
        $account = new Account();
        $account->setNumber('2222233333');
        $account->setName('Novák, Tomáš');
        $account->setBankId('2010');
        $account->setBankName('Fio banka, a.s.');
        $account->setBic('UNCRITMMX');
        $transaction->setRecipient($account);

        $expectedTransactions[] = $transaction;

        $this->assertEquals($expectedTransactions, $transactions);
    }
}