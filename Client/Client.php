<?php

/*
* This file is part of the Fio Bank Account Bundle.
*
* (c) Petr Jaroš <petajaros@gmail.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace SimplyNet\FioBankTransferBundle\Client;

use JMS\Payment\CoreBundle\Plugin\Exception\FunctionNotSupportedException;
use JMS\Payment\CoreBundle\Plugin\Exception\CommunicationException;

abstract class Client
{
    /**
     * @var string 
     */
    private $uri = 'https://www.fio.cz/ib_api/rest/';
    
    /**
     * @var string
     */
    private $token;
    
    /**
     * @var string
     */
    private $type;
    
    /**
     * @var string 
     */
    private $rootCertificate;

    /**
     * 
     * @param string $type
     * @param string $token
     * @param string $rootCertificate
     */
    function __construct($type, $token, $rootCertificate = null)
    {
        $this->type = $type;
        $this->token = $token;
        $this->rootCertificate = $rootCertificate;
    }

    /**
     * 
     * @param \DateTime $fromDate
     * @param \DateTime $toDate
     * @throws FunctionNotSupportedException
     */
    public function getTransactionsByDate(\DateTime $fromDate, \DateTime $toDate) {
        throw new FunctionNotSupportedException;
    }

    /**
     * 
     * @param int $year
     * @param int $id
     * @throws FunctionNotSupportedException
     */
    public function getTransactionsByOrderNumber($year, $id) {
        throw new FunctionNotSupportedException;
    }

    /**
     * 
     * @throws FunctionNotSupportedException
     */
    public function getLastTransactions() {
        throw new FunctionNotSupportedException;
    }

    /**
     * 
     * @param int $id
     */
    public function setLastTransactionById($id) {
        $resource = 'set-last-id/' . $this->token . '/' . $id . '/';
        $this->getRequest($resource);
    }

    /**
     * 
     * @param \DateTime $date
     */
    public function setLastTransactionByDate(\DateTime $date) {
        $resource = 'set-last-date/' . $this->token . '/' . $date->format('Y-m-d') . '/';
        $this->getRequest($resource);
    }

    /**
     * 
     * @param \DateTime $fromDate
     * @param \DateTime $toDate
     * @return string
     */
    public function getRequestTransactionsByDate(\DateTime $fromDate, \DateTime $toDate) {
        $resource = 'periods/' . $this->token . '/' . $fromDate->format('Y-m-d') . '/' . $toDate->format('Y-m-d') . '/transactions' . '.' . $this->type;
        return $this->getRequest($resource);
    }

    /**
     * 
     * @param int $year
     * @param int $orderNumber
     * @return string
     */
    public function getRequestTransactionsByOrderNumber($year, $orderNumber) {
        $resource = 'by-id/' . $this->token . '/' . $year . '/' . $orderNumber . '/transactions' . '.' . $this->type;
        return $this->getRequest($resource);
    }

    /**
     * 
     * @return string
     */
    public function getRequestLastTransactions() {
        $resource = 'last/' . $this->token . '/' . 'transactions' . '.' . $this->type;
        return $this->getRequest($resource);
    }

    /**
     * 
     * @param string $resource
     * @param string $urlSuffix
     * @return string
     */
    protected function getRequest($resource) {
        if (!extension_loaded('curl')) {
            throw new \RuntimeException('The cURL extension must be loaded.');
        }

        $url = $this->uri . $resource;
        $curl = \curl_init($url);

        \curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if (is_file($this->rootCertificate)) {
            \curl_setopt($curl, CURLOPT_CAINFO, $this->rootCertificate);
        } else {
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        }
        \curl_setopt($curl, CURLOPT_HEADER, 0);

        if (false === $response = curl_exec($curl)) {
            throw new CommunicationException(
            'cURL Error: ' . curl_error($curl), curl_errno($curl)
            );
        }

        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($httpCode !== 200) {
            throw new CommunicationException(
            'Communication Error: HTTP response code is ' . $httpCode, $httpCode
            );
        }
        \curl_close($curl);
        return $response;
    }

}
