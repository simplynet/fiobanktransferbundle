Fio Bank Transfer Client
===================

This is a client to Fio bank banking API.

[![Build Status](https://drone.io/bitbucket.org/simplynet/fiobanktransferclient/status.png)](https://drone.io/bitbucket.org/simplynet/fiobanktransferclient/latest)
