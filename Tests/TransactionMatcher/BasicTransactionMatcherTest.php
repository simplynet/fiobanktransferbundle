<?php

/* 
 * This file is part of the Fio Bank Account Bundle.
 * 
 * (c) Petr Jaroš <petajaros@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SimplyNet\FioBankTransferBundle\Tests\TransactionMatcher;

use SimplyNet\FioBankTransferBundle\TransactionMatcher\BasicTransactionMatcher;
use JMS\Payment\CoreBundle\Entity\PaymentInstruction;
use JMS\Payment\CoreBundle\Entity\Payment;
use SimplyNet\FioBankTransferBundle\Client\Transaction;

class BasicTransactionMatcherTest extends \PHPUnit_Framework_TestCase
{
    public function testMarkPaymentInstruction()
    {
        $transactionMatcher = new BasicTransactionMatcher();
        $paymentInstruction = new PaymentInstruction(500, 'CZK', 'fio_bank_transfer');
        $transactionMatcher->markPaymentInstruction($paymentInstruction);

        $this->assertStringMatchesFormat('%d', $paymentInstruction->getExtendedData()->get('variable_symbol'));
    }

    public function testMarkPaymentInstructionKeepOriginalVariableSymbol()
    {
        $transactionMatcher = new BasicTransactionMatcher();
        $paymentInstruction = new PaymentInstruction(500, 'CZK', 'fio_bank_transfer');
        $paymentInstruction->getExtendedData()->set('variable_symbol', '1234567890');
        $transactionMatcher->markPaymentInstruction($paymentInstruction);

        $this->assertEquals('1234567890', $paymentInstruction->getExtendedData()->get('variable_symbol'));
    }

    /**
     * @dataProvider matchDataProvider
     */
    public function testMatch($paymentVariableSymbol, $paymentSpecificSymbol, $paymentAmount,
            $transactionVariableSymbol, $transactionSpecificSymbol, $transactionAmount, $match)
    {
        $transactionMatcher = new BasicTransactionMatcher();
        $paymentInstruction = new PaymentInstruction($paymentAmount, 'CZK', 'fio_bank_transfer');
        if($paymentVariableSymbol !== null) $paymentInstruction->getExtendedData()->set('variable_symbol', $paymentVariableSymbol);
        if($paymentSpecificSymbol !== null) $paymentInstruction->getExtendedData()->set('specific_symbol', $paymentSpecificSymbol);
        $payment = new Payment($paymentInstruction, $paymentAmount);
        $payment->setDepositingAmount($paymentAmount);
        $transaction = new Transaction();
        if($transactionVariableSymbol) $transaction->setVariableSymbol($transactionVariableSymbol);
        if($transactionSpecificSymbol) $transaction->setSpecificSymbol($transactionSpecificSymbol);
        $transaction->setAmount($transactionAmount);

        $this->assertEquals($match, $transactionMatcher->match($transaction, $payment));
    }

    public function matchDataProvider()
    {
        return array(
            array('1234567890', null, 500, '1234567890', null, 500, true),
            array(null, '1234567890', 500, null, '1234567890', 500, true),
            array('1234567890', '1234567899', 300, '1234567890', '1234567899', 300, true),
            array('1234567890', null, 500.50, '1234567890', null, 500.5, true),
            array('1234567890', null, 500.0000001, '1234567890', null, 500.00, true),
            array('1234567890', null, 500, '1234567890', null, 299, false),
            array('1234567890', null, 500, '9999999999', null, 299, false),
            array(null, '1234567890', 500, null, '9990999999', 500, false),
            array('999999999', '1234567899', 300, '1234567890', '1234567899', 300, false),
            array('1234567890', '001234567899', 300, '001234567890', '1234567899', 300, true),
            array('  1234567890', '1234567899', 300, '1234567890', '   1234567899', 300, true),
        );
    }
}