<?php

/*
 * This file is part of the Fio Bank Account Bundle.
 *
 * (c) Petr Jaroš <petajaros@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SimplyNet\FioBankTransferBundle\Tests\Functional\Client;

use SimplyNet\FioBankTransferBundle\Tests\Functional\KernelTestCase;

class ClientTest extends KernelTestCase
{
    /**
     * @var \SimplyNet\FioBankTransferBundle\Client\XmlClient
     */
    private $client;

    public function setUp()
    {
        static::bootKernel();
        $token = static::$kernel->getContainer()->getParameter('simply_net.fio_bank_transfer.token');
        $rootCertificate = static::$kernel->getContainer()->getParameter('simply_net.fio_bank_transfer.root_certificate');
        $this->client = $this->getMockForAbstractClass('\SimplyNet\FioBankTransferBundle\Client\Client', array('xml', $token, $rootCertificate));
    }

    public function testGetRequestTransactionsByDate()
    {
        sleep(30);
        $data = $this->client->getRequestTransactionsByDate(new \DateTime('2012-01-01'), new \DateTime('2012-01-01'));
        $this->assertNotEmpty($data);
    }

}