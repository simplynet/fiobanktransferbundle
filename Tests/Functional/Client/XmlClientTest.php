<?php

/* 
 * This file is part of the Fio Bank Account Bundle.
 * 
 * (c) Petr Jaroš <petajaros@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SimplyNet\FioBankTransferBundle\Tests\Functional\Client;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class XmlClientTest extends KernelTestCase
{
    /**
     * @var \SimplyNet\FioBankTransferBundle\Client\XmlClient
     */
    private $xmlClient;

    public function setUp()
    {
        static::bootKernel();
        $this->xmlClient = static::$kernel->getContainer()->get('simply_net.fio_bank_transfer.client.xml');
    }

    public function testGetRequestTransactionsByDate()
    {
        sleep(30);
        $xml = $this->xmlClient->getRequestTransactionsByDate(new \DateTime('2012-01-01'), new \DateTime('2012-01-01'));
        $xmlObject = new \SimpleXMLElement($xml);
        $xmlInfo = $xmlObject->xpath('/AccountStatement/Info');
        $this->assertEquals(1, count($xmlInfo));
    }

    public function testGetTransactionsByDate()
    {
        sleep(30);
        $transactions = $this->xmlClient->getTransactionsByDate(new \DateTime('2012-01-01'), new \DateTime('2012-01-01'));
        $this->assertInternalType('array', $transactions);
    }
}