<?php

/*
* This file is part of the Fio Bank Account Bundle.
*
* (c) Petr Jaroš <petajaros@gmail.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace SimplyNet\FioBankTransferBundle\Tests\Functional\Plugin;

use SimplyNet\FioBankTransferBundle\Tests\Functional\KernelTestCase;
use JMS\Payment\CoreBundle\Entity\PaymentInstruction;
use JMS\Payment\CoreBundle\Model\FinancialTransactionInterface;
use JMS\Payment\CoreBundle\Model\PaymentInterface;
use JMS\Payment\CoreBundle\PluginController\Result;
use Doctrine\ORM\Tools\SchemaTool;

class FioBankTransferPluginTest extends KernelTestCase
{
    /**
     * @var \JMS\Payment\CoreBundle\PluginController\PluginController
     */
    private $pluginController;

    public function setUp()
    {
        static::bootKernel();
        $this->pluginController = static::$kernel->getContainer()->get('payment.plugin_controller');
        $em = static::$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $schemaTool = new SchemaTool($em);
        $metadata = $em->getMetadataFactory()->getAllMetadata();
        $schemaTool->createSchema($metadata);
    }

    public function testApproveAndDeposit()
    {
        $paymentInstruction = new PaymentInstruction(500, 'CZK', 'fio_bank_transfer');
        $this->pluginController->createPaymentInstruction($paymentInstruction);
        $payment = $this->pluginController->createPayment($paymentInstruction->getId(), 500);
        $result = $this->pluginController->approveAndDeposit($payment->getId(), 500);
        $this->assertEquals(FinancialTransactionInterface::STATE_PENDING, $result->getFinancialTransaction()->getState());
        $this->assertEquals(Result::STATUS_PENDING, $result->getStatus());
        $this->assertInstanceOf('JMS\Payment\CoreBundle\Plugin\Exception\PaymentPendingException', $result->getPluginException());

        $payment->getPendingTransaction()->setReferenceNumber('12345');
        $this->pluginController->approveAndDeposit($payment->getId(), 500);
        $this->assertEquals(FinancialTransactionInterface::STATE_SUCCESS, $result->getFinancialTransaction()->getState());
        $this->assertEquals(PaymentInterface::STATE_DEPOSITED, $payment->getState());
    }
}
