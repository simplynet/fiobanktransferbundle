<?php
namespace SimplyNet\FioBankTransferBundle\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase as BaseKernelTestCase;

abstract class KernelTestCase extends BaseKernelTestCase
{
    /**
     * @return string
     */
    public static function getKernelClass()
    {
        require_once __DIR__ . '/app/AppKernel.php';

        return 'AppKernel';
    }
}
