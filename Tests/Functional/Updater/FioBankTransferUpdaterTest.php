<?php

/*
* This file is part of the Fio Bank Account Bundle.
*
* (c) Petr Jaroš <petajaros@gmail.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace SimplyNet\FioBankTransferBundle\Tests\Functional\Plugin;

use SimplyNet\FioBankTransferBundle\Tests\Functional\KernelTestCase;
use SimplyNet\FioBankTransferBundle\Updater\FioBankTransferUpdater;
use JMS\Payment\CoreBundle\Entity\PaymentInstruction;
use JMS\Payment\CoreBundle\Model\PaymentInterface;
use JMS\Payment\CoreBundle\Entity\ExtendedData;
use Doctrine\ORM\Tools\SchemaTool;

class FioBankTransferUpdaterTest extends KernelTestCase
{
    /**
     * @var \JMS\Payment\CoreBundle\PluginController\PluginController
     */
    private $pluginController;

    public function setUp()
    {
        static::bootKernel();
        $this->pluginController = static::$kernel->getContainer()->get('payment.plugin_controller');
        $em = static::$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $schemaTool = new SchemaTool($em);
        $metadata = $em->getMetadataFactory()->getAllMetadata();
        $schemaTool->createSchema($metadata);
    }

    public function testCheckPayments()
    {
        $xml = file_get_contents(__DIR__ . '/../../Fixtures/incomingPayments.xml');

        $client = $this->getMock(
                '\SimplyNet\FioBankTransferBundle\Client\XmlClient',
                array('getRequestLastTransactions'),
                array('')
        );

        $client->method('getRequestLastTransactions')
                ->willReturn($xml);

        $em = static::$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $transactionManager = static::$kernel->getContainer()->get('simply_net.fio_bank_transfer.transaction_matcher');
        $updater = new FioBankTransferUpdater($this->pluginController, $client, $transactionManager, $em);

        $extendedData = new ExtendedData();
        $extendedData->set('variable_symbol', '3435555343');
        $paymentInstruction = new PaymentInstruction(500, 'CZK', 'fio_bank_transfer', $extendedData);
        $this->pluginController->createPaymentInstruction($paymentInstruction);
        $payment = $this->pluginController->createPayment($paymentInstruction->getId(), 500);
        $this->pluginController->approveAndDeposit($payment->getId(), 500);
        $processedPayments = array();
        $unprocessedPayments = array();
        $updater->updatePayments($processedPayments, $unprocessedPayments);
        $this->assertEquals(PaymentInterface::STATE_DEPOSITED, $payment->getState());
        $this->assertCount(1, $processedPayments);
        $this->assertCount(1, $unprocessedPayments);
    }
}
