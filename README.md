FioBankTransferBundle
=====================

This is a [Symfony](http://symfony.com) bundle and an extension for [JMSPaymentCoreBundle](http://jmsyst.com/bundles/JMSPaymentCoreBundle) providing access to [Fio bank](http://www.fio.cz) banking API but it also can be used outside of Symfony framework.

[![Build Status](https://drone.io/bitbucket.org/simplynet/fiobanktransferbundle/status.png)](https://drone.io/bitbucket.org/simplynet/fiobanktransferbundle/latest)

## Installation as Symfony2 bundle ##

### 1. Download FioBankTransferBundle using Composer###

Add FioBankTransferBundle by running the command:

```
#!php

$ php composer.phar require friendsofsymfony/user-bundle 'dev-master'
```

Composer will install the bundle to your project's vendor/simplynet directory.

### 2. Enable the bundle ###

Enable the bundle in the kernel:

```
#!php

<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new SimplyNet\FioBankTransferBundle\SimplyNetFioBankTransferBundle(),
    );
}
```

### 3. Configure the FioBankTransferBundle ###

You need to get your token from your online banking and put it in your configuration file:

```
#!yaml

# app/config/config.yml
simply_net_fio_bank_transfer:
    token: your-token
```
or if you prefer xml:

```
#!xml

<!-- app/config/config.xml -->
<simply_net_fio_bank_transfer:config
    token="your-token"
/>
```

## Testing ##

You need to set environment variable `SYMFONY__FIO_BANK_TRANSFER__TOKEN` to your Fio banking API token in order to run functional tests.

Current script for running tests is available on [drone.io](https://drone.io/bitbucket.org/simplynet/fiobanktransferbundle/admin).
